#ifndef IMEAN_H
#define IMEAN_H

template <typename GENERIC_TYPE>

class IMean
	{
	public:
		/**
		 * param1 		Data[data.size()]	 	
		 * param2		Clusters[data.size()]		Wich cluster each row in data belongs to
		 * param3		NumberOfClusters		
		 * param4		Centroids[NumberOfClusters] 	Centerpoint for each cluster
		 */	
		virtual vector<GENERIC_TYPE> Mean(vector<GENERIC_TYPE> data, int *clusters, int count) = 0;
	};
	
#endif
