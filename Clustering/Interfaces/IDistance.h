#ifndef IDISTANCE_H
#define IDISTANCE_H

template <typename GENERIC_TYPE>

class IDistance
	{
	public:
		virtual double GetDistance(GENERIC_TYPE t1, GENERIC_TYPE t2) = 0;
	};
	
#endif
