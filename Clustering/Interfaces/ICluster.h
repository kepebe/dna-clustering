#ifndef ICLUSTER_H
#define ICLUSTER_H

template<typename GENERIC_TYPE>

class ICluster
	{
	public:
		bool Multithreaded = false;
		virtual int* Cluster(std::vector<GENERIC_TYPE> data) = 0;
		virtual void Initialize(IDistance<GENERIC_TYPE>* dist, Parameter* params) = 0;
	};
	
#endif
