#ifndef IDATAREADER_H
#define IDATAREADER_H

#include <vector>

template <typename GENERIC_TYPE>

class IDataReader
	{
	public:
		virtual std::vector<GENERIC_TYPE> ReadData(std::ifstream* input) = 0;
	};
	
#endif
