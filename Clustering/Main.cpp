#include <sstream>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <thread>
#include <bitset>
#include <map>
#include <vector>
#include <algorithm>
using namespace std;

#include "Utilities/getRSS.c"
#include "Utilities/TimeProbe.h"
#include "Utilities/Parameter.h"
#include "Interfaces/IDistance.h"
#include "Interfaces/ICluster.h"
#include "Interfaces/IDataReader.h"
#include "Interfaces/IMean.h"
#include "Model/Point.h"
#include "Model/DNA.h"

int dataType = 0;
int algorithm = 0;
int splitAlgorithm = -1;
int hybridAlgorithm = -1;
int splitSize = 1000;
int distanceType = 0;
int parallel = 1;
string inputFileName = "input.txt";
string outputFileName = "output.txt";

#include "Utilities/Misc.h"
#include "Implementations/DensityBased.h"
#include "Implementations/ConnectivityBased.h"
#include "Implementations/TreeBased.h"
#include "Implementations/CentroidBased.h"

ICluster<char*>* DNAAlgorithms[] =
	{
	new DensityCluster<char*>(),
	new ConnectivityCluster<char*>(OrderDNA),
	new CentroidCluster<char*>(new DNAMean()),
	new TreeCluster()
	};
	
IDistance<char*>* DNADistances[] =
	{
	new DNADistance(),
	new DNARelativeDistance(),
	new DNADynamicDistance()
	};
	
ICluster<Point>* PointAlgorithms[] =
	{
	new DensityCluster<Point>(),
	new ConnectivityCluster<Point>(NULL),
	new CentroidCluster<Point>(new PointMean())
	};
	
IDistance<Point>* PointDistances[] =
	{
	new EuclidianDistance()
	};

#include "Implementations/SplitInput.h"
#include "Implementations/Hybrid.h"

void ParseArguments(int argc, char* argv[])
	{
	for (int i = 1; i < argc; i++)
		{
		if (argv[i][0] == '-')
			{
			switch (argv[i][1])
				{
				case 'i'://input
					i++;
					inputFileName = argv[i];
					break;
				case 'o'://output
					i++;
					outputFileName = argv[i];
					break;
				case 't'://datatype
					i++;
					dataType = atoi(argv[i]);
					break;
				case 'a'://algorithm
					i++;
					algorithm = atoi(argv[i]);
					break;
				case 's'://split algorithm
					i++;
					splitAlgorithm = atoi(argv[i]);
					break;
				case 'h'://hybrid algorithm
					i++;
					hybridAlgorithm = atoi(argv[i]);
					break;
				case 'c'://split size
					i++;
					splitSize = atoi(argv[i]);
					break;
				case 'p'://Parallel
					i++;
					parallel = atoi(argv[i]);
					break;
				case 'd'://distance
					i++;
					distanceType = atoi(argv[i]);
					break;
				case 'n'://parameters for main algorithm
					i++;
					LoadParameters(argv[i], &parameters1);
					break;
				case 'm'://parameters for split algorithm
					i++;
					LoadParameters(argv[i], &parameters2);
					break;
				}
			}
		}
	}
	
ICluster<char*>* GetDNAAlgorithm(int alg, int dist, Parameter* params)
	{
	ICluster<char*>* clustering = DNAAlgorithms[alg];
	
	IDistance<char*>* distance = DNADistances[dist];
	
	clustering->Initialize(distance, params);
	
	return clustering;
	}
	
ICluster<Point>* GetPointAlgorithm(int alg, int dist, Parameter* params)
	{
	ICluster<Point>* clustering = PointAlgorithms[alg];
	
	IDistance<Point>* distance = PointDistances[dist];
	
	clustering->Initialize(distance, params);
	
	return clustering;
	}

int main(int argc, char* argv[])
	{
	ParseArguments(argc, argv);
	Parameter* param1 = new Parameter(parameters1);
	Parameter* param2 = new Parameter(parameters2);
	ifstream inStream;
	inStream.open(inputFileName);
	switch (dataType)
		{
		case 0:
			{
			cout << "Using DNA datatype" << endl;
			if (splitAlgorithm > -1)
				{
				ICluster<char*>* main = GetDNAAlgorithm(algorithm, distanceType, param1);
				ICluster<char*>* merge = GetDNAAlgorithm(splitAlgorithm, distanceType, param2);
				main->Multithreaded = true;
				merge->Multithreaded = true;
				cout << "Splitting input file by " << splitSize << endl;
				TimeProbeStart("Clustering", true);
				RunSplit(splitSize, &inStream, main, merge, parallel);
				TimeProbeEnd(true);
				}
			else if (hybridAlgorithm > -1)
				{
				ICluster<char*>* main = GetDNAAlgorithm(algorithm, distanceType, param1);
				ICluster<char*>* coarse = GetDNAAlgorithm(hybridAlgorithm, distanceType, param2);
				main->Multithreaded = true;
				coarse->Multithreaded = true;
				Hybrid hybrid(&inStream, parallel);
				int* clusters;
				TimeProbeStart("Clustering", true);
				clusters = hybrid.Cluster(coarse, main);
				TimeProbeEnd(true);
				OutputResult(clusters, hybrid.size());		
				}
			else
				{
				FostaFileReader reader;
				vector<char*> data = reader.ReadData(&inStream);
				ICluster<char*>* clustering = GetDNAAlgorithm(algorithm, distanceType, param1);
				TimeProbeStart("Clustering", true);
				int* clusters = clustering->Cluster(data);
				TimeProbeEnd(true);
				OutputResult(clusters, data.size());
				break;
				}
			break;
			}
		case 1:
			{
			cout << "Using point datatype" << endl;
			PointFileReader reader;
			vector<Point> data = reader.ReadData(&inStream);
			ICluster<Point>* clustering = GetPointAlgorithm(algorithm, distanceType, param1);
			TimeProbeStart("Clustering", true);
			int* clusters = clustering->Cluster(data);
			TimeProbeEnd(true);
			OutputResult(clusters, data.size());
			break;
			}
		}
	cout << "Used " << getPeakRSS()/1024/1024 << " MB of space" << endl;
	inStream.close();
	WriteAllTimeAgg();
	}
