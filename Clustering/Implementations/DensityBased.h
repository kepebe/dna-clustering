using namespace std;
	
template<typename GENERIC_TYPE>

class DensityCluster : public ICluster<GENERIC_TYPE>
	{
	public:
		void Initialize(IDistance<GENERIC_TYPE>* dist, Parameter* params)
			{
			_distance = dist;
			_threshold = 1 - params->GetParameter(0, 0.97);
			_minPts = (int)params->GetParameter(0, 4.0);
			}
			
		int* Cluster(vector<GENERIC_TYPE> data)
			{
			int C = -1;
			int count;
			TimeProbeStart("Converting");
			vector<DataPoint*> points = Convert(data);
			TimeProbeEnd();
			for (int i = 0; i < points.size(); i++)
				{
				if (!points[i]->Visited)
					{
					points[i]->Visited = true;
					Node* np = RegionQuery(points[i], points, &count);
					if (count < _minPts)
						{
						points[i]->Cluster = -1;
						}
					else
						{
						C++;
						TimeAggStart("Expand cluster");
						ExpandCluster(points[i], np, C, points);
						TimeAggStop("Expand cluster");
						}
					}
				WriteProgress(i, data.size(), C, this);
				}
			
			cout << endl;
			
			return GetResult(points);
			}
			
	private:
		typedef struct
			{
			GENERIC_TYPE p;
			int Cluster;
			bool Visited;
			} DataPoint;
			
		typedef struct node
			{
			DataPoint* p;
			node* Next;
			node* Previous;
			} Node;
			
		IDistance<GENERIC_TYPE>* _distance;
		double _threshold;
		int _minPts;
		
		void Append(Node* head, Node* tail)
			{
			if (head->Next == NULL)
				{
				head->Next = tail;
				head-> Previous = tail;
				tail->Previous = head;
				}
			else
				{
				tail->Previous = head->Previous;
				head->Previous->Next = tail;
				head->Previous = tail;
				}
			tail->Next = head;
			}
			
		void AppendList(Node* head, Node* head2)
			{
			if (head->Next == NULL && head2->Next == NULL)
				{
				Append(head, head2);
				}
			else if (head2->Next == NULL)
				{
				Append(head, head2);
				}
			else if (head->Next == NULL)
				{
				head->Next = head2;
				head->Previous = head2->Previous;
				head2->Previous = head;
				}
			else
				{
				head->Previous->Next = head2;
				head->Previous = head2->Previous;
				head2->Previous->Next = head;
				}
			}
		
		vector<DataPoint*> Convert(vector<GENERIC_TYPE> data)
			{
			vector<DataPoint*> result(data.size());
			for (int i = 0; i < data.size(); i++)
				{
				result[i] = new DataPoint();
				result[i]->p = data[i];
				result[i]->Visited = false;
				result[i]->Cluster = -1;
				}
			return result;
			}
			
		int* GetResult(vector<DataPoint*> p)
			{
			int* result = new int[p.size()];
			for (int i = 0; i < p.size(); i++)
				{
				result[i] = p[i]->Cluster;
				}
			return result;
			}
		
		void ExpandCluster(DataPoint* p, Node* np, int C, vector<DataPoint*> data)
			{
			p->Cluster = C;
			Node* CurrentNode = np;
			int count;
			do 
				{
				if (!CurrentNode->p->Visited && CurrentNode->p->Cluster != C)
					{
					CurrentNode->p->Visited = true;
					Node* np2 = RegionQuery(CurrentNode->p, data, &count);
					if (count >= _minPts)
						{
						AppendList(np, np2);
						}
					}
				if (CurrentNode->p->Cluster < 0)
					{
					CurrentNode->p->Cluster = C;
					}
				CurrentNode = CurrentNode->Next;
				} while (CurrentNode != np);
			}
		
		Node* RegionQuery(DataPoint* p, vector<DataPoint*> data, int* count)
			{
			TimeAggStart("Region query");
			Node* result = new Node();
			result->p = p;
			int j = 0;
			for (int i = 0; i < data.size(); i++)
				{
				if (p != data[i] && _distance->GetDistance(p->p, data[i]->p) < _threshold)
					{
					Node* next = new Node();
					next->p = data[i];
					Append(result, next);
					j++;
					}
				}
			*count = j;
			TimeAggStop("Region query");
			return result;
			}
	};
