using namespace std;

char CharFromInt[] = {2, 1, 4, 8};

class TreeCluster : public ICluster<char*>
	{
	public:	
		void Initialize(IDistance<char*>* dist, Parameter* params)
			{
			_maxLength = 1 - params->GetParameter(0, 0.97);
			}
			
		int* Cluster(vector<char*> data)
			{
			int* result = new int[data.size()];
			//Find number between minimum and maximum string length
			for (int i = 0; i < data.size(); i++)
				{
				result[i] = -1;
				}

			//Construct tree, get list of leaves
			TimeProbeStart("Construcing tree");
			vector<TreeNode*> leaf = ConstructTree(&data);
			TreeNode* root = leaf[0];
			sort(leaf.begin(), leaf.end(), SortFunc);
			TimeProbeEnd();
			int cluster = 0; 
			
			//Run up tree, one leave at a time, skipping already clustered leaves
			TimeProbeStart("Traversing tree");
			for (int i = 0; i < leaf.size(); i++)
				{
				if (leaf[i]->Parent != NULL && !leaf[i]->Visited)
					{
					char* DNA = data[leaf[i]->Strings[2]];
					RunUpTree(leaf[i], DNA, result, *((int*)DNA) - 1, true, cluster);
					cluster++;
					}
				WriteProgress(i+1, data.size(), cluster, this);
				}
			TimeProbeEnd();
			if (Multithreaded)
				{
				CleanTree(root);
				}
			return result;
			}
			
	private:
		double _maxLength;
		typedef struct treeNode
			{
			bool Visited;
			int* Strings;
			treeNode* Children[4];
			treeNode* Parent;
			} TreeNode;
			
		//Creates a node with a parent(can be NULL) and a character value
		TreeNode* CreateNode(TreeNode* parent)
			{
			TreeNode* newNode = new TreeNode();
			newNode->Parent = parent;
			newNode->Visited = false;
			newNode->Strings = NULL;
			for (int i = 0; i < 4; i++)
				{
				newNode->Children[i] = NULL;
				}
			return newNode;
			}
			
		static bool SortFunc(TreeNode* t1, TreeNode* t2)
			{
			if (t1->Parent == NULL)
				{
				return true;
				}
			if (t2->Parent == NULL)
				{
				return false;
				}
			return t1->Strings[1] > t2->Strings[1];
			}
			
		//Constructs a tree based on a list of DNA strings
		//The result is a linked list of leaf nodes where:
		//The first half has length > midLength and the rest below
		vector<TreeNode*> ConstructTree(vector<char*>* data)
			{
			TreeNode* root = CreateNode(NULL);
			vector<TreeNode*> result;
			result.push_back(root);
			for (int i = 0;i < data->size(); i++)
				{
				int length = *((int*)data->at(i));
				result.push_back(RunDown(data->at(i), 4, *((int*)data->at(i)), root, i));
				}
			return result;
			}
		
		//Convert a character to an index for the treenodes
		int IntFromChar(char ch)
			{
			if (ch & 1)
				{
				return 1;//A
				}
			if (ch & 2)
				{
				return 0;//T
				}
			if (ch & 4)
				{
				return 2;//G
				}
			if (ch & 8)
				{
				return 3;//C
				}
			throw runtime_error("Got invalid character: " + to_string((int)ch));
			}
			
		bool CanGo(char ch, int child)
			{
			return ch & CharFromInt[child];
			}
			
		void CleanTree(TreeNode* node)
			{
			for (int i = 0; i < 4; i++)
				{
				if (node->Children[i] != NULL)
					{
					CleanTree(node->Children[i]);
					}
				}
			if (node->Strings != NULL)
				{
				delete[] node->Strings;
				}
			delete node;
			}

		//Construct a branch in the tree based on a DNA string, making new nodes as we go
		TreeNode* RunDown(char* DNA, int num, int length, TreeNode* parent, int string)
			{
			char value = DNA[num];
			int idx = IntFromChar(value);
			TreeNode* newNode;
			if (parent->Children[idx] == NULL)
				{
				newNode = CreateNode(parent);
				parent->Children[idx] = newNode;
				}
			else
				{
				newNode = parent->Children[idx];
				}
			if (num == length - 1)
				{
				if (newNode->Strings == NULL)
					{
					newNode->Strings = new int[3];
					newNode->Strings[0] = 1;
					newNode->Strings[1] = length;
					newNode->Strings[2] = string;
					}
				else
					{
					int len = newNode->Strings[0];
					int* tmp = new int[len + 3];
					for (int i = 1; i < len + 2; i++)
						{
						tmp[i] = newNode->Strings[i];
						}
					tmp[0] = len + 1;
					tmp[len + 2] = string;
					delete[] newNode->Strings;
					newNode->Strings = tmp;
					}
				return newNode;
				}
			else
				{
				return RunDown(DNA, num + 1, length, newNode, string);
				}
			}
		
		//Run down the tree from a specific node, noting how many wrong turns has been made
		//Puts all DNA strings belonging to the treenode in the same cluster 
		void RunDownTree(TreeNode* leaf, char* DNA, int* groups, int place, int cluster, int error)
			{
			if ((error/(double)place) > _maxLength)
				{
				return;
				}
			leaf->Visited = true;

			if (leaf->Strings != NULL)
				{
				for (int i = 0; i < leaf->Strings[0]; i++)
					{
					groups[leaf->Strings[i + 2]] = cluster;
					}
				}

			for (int i = 0;i < 4; i++)
				{
				char value = DNA[place + 1];
				if (leaf->Children[i] != NULL)
					{
					bool isValid = CanGo(value, i);
					RunDownTree(leaf->Children[i], DNA, groups, place + 1, cluster, error + (isValid ? 0 : 1));
					}
				}
			}
		
		//Run up tree and put all DNA strings we encounter in a cluster
		//Will run back down the tree to include new DNA strings in its cluster
		void RunUpTree(TreeNode* leaf, char* DNA, int* groups, int place, bool first, int cluster)
			{
			leaf->Visited = true;

			if (leaf->Strings != NULL)
				{
				for (int i = 0; i < leaf->Strings[0]; i++)
					{
					groups[leaf->Strings[i + 2]] = cluster;
					}
				}

			if (!first)
				{
				int idx = IntFromChar(DNA[place + 1]);
				char value = DNA[place + 1];
				for (int i = 0;i < 4; i++)
					{
					bool isValid = CanGo(value, i);
					if (i!=idx && leaf->Children[i] != NULL)
						{
						RunDownTree(leaf->Children[i], DNA, groups, place + 1, cluster, isValid ? 0 : 1);
						}
					}
				}
			if (place >= 4)
				{
				RunUpTree(leaf->Parent, DNA, groups, place - 1, false, cluster);
				}
			}
	};

