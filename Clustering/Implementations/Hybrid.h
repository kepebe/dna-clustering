using namespace std;

class Hybrid
	{
	public:
		Hybrid(ifstream* input, int parallel)
			{
				FostaFileReader reader;	
				_data 		= reader.ReadData(input);
				_parallel 	= parallel;
			}

		int size()
			{
				return _data.size();
			}

		int* Cluster(ICluster<char*>* _rough,
		ICluster<char*>* _main)
			{
			cout << "Clustering: " << _data.size() << endl;
			int* result = _rough->Cluster(_data);
			vector<CData> indata;			
			cout << "Rough clustering" << endl;
			indata = Merge(result);
			cout << "Rough clusters: " << indata.size() << endl;
			vector<int*> resdata(indata.size());
			
			if(_parallel > 0)
				{
				thread *threads;
				threads = new thread[_parallel];
				int activethreads = 0;
				for(int i = 0; i < indata.size(); i++)
					{
					if(activethreads == (_parallel))
						{
						for(int j = 0; j < _parallel; j++)
							{
							threads[j].join();
							}
						activethreads = 0;
						}
					threads[activethreads] = thread(&Hybrid::TCluster,
							this, &indata[i], _main, &resdata, i);
					activethreads++;
					}
				for(int i = 0; i < activethreads; i++)
					{
					threads[i].join();
					}	
				}
				else
				{
				for(int i = 0; i < indata.size(); i++)
					{
					TCluster(&indata[i], _main, &resdata, i);
					}
				}
			
			return Mergeresults(&indata, resdata, _data.size());
			}

	private:
		int 			_parallel;
		vector<char*>	_data;

		typedef struct {
			vector<char*> data;
			vector<int>   line;
			vector<int>   cluster;
		} CData;

		int* Mergeresults(vector<CData >* data, vector<int*> resdata, int totalrows)
			{
			int ccount = 0;
			map<int, int>::iterator p;
			int* res = new int[totalrows];

			for(int i = 0; i < data->size(); i++)
				{
				map<int, int> clusters;					
				vector<int> line = data->at(i).line;
				for(int j = 0; j < data->at(i).data.size(); j++)
					{
					if( (p = clusters.find(resdata[i][j])) != clusters.end() )
						{
						res[line[j]] = p->second;
						}
					else
						{
						clusters[resdata[i][j]] = ccount;
						res[line[j]] = ccount;
						ccount++;
						}
					}
				}
			return res;
			}
		
		void TCluster(CData *data, ICluster<char*>* clustering,	vector<int*>* resData, int idx)
			{
			(*resData)[idx] = clustering->Cluster(data->data);
			}		

		vector<CData> Merge(int* result)
			{
			map<int, int> position;
			map<int, int>::iterator p;
			int c = 0;
			vector<CData> sdata;
			for(int i = 0; i < _data.size(); i++)
				{
				int r = result[i];				
				if( (p = position.find(r)) != position.end() )
					{
					sdata[p->second].data.push_back(_data[i]);
					sdata[p->second].line.push_back(i);
					sdata[p->second].cluster.push_back(p->second);
					}
				else
					{
					position[r] = c;
					CData *newd = new CData();
					newd->data.push_back(_data[i]);
					newd->line.push_back(i);
					newd->cluster.push_back(c);
					sdata.push_back(*newd);
					c++;
					}
				}
			return sdata;
			}
	};
