using namespace std;

template<typename GENERIC_TYPE>

/** 
 * This class implements the:
 * Clustering algorithm K means
 */
class CentroidCluster : public ICluster<GENERIC_TYPE>
	{
	public:
		/**
		 * param1 	IDistance			Calculate the distance between to GENERIC_TYPE values
		 * param2	calculateCentroids	Calculate the mean for all GENERICT_TYPE values in each cluster
		 * param3	NumberOfCluster	
		 * param4	Iterations			Maximum times centroid mean is calculated
		 * param5	threads				number of threads or 0 for no threads.
		 */
		CentroidCluster(IMean<GENERIC_TYPE> *centroidMean)
			{
			_calCentroids 		= centroidMean;
			srand (time(NULL));
			}		
			
		void Initialize(IDistance<GENERIC_TYPE>* dist, Parameter* params)
			{
			_distance  		= dist;
			_count = (int)params->GetParameter(0, 5.0);
			_iter = (int)params->GetParameter(0, 3.0);
			_threads = (int)params->GetParameter(0, -1.0);
			_threads = _threads < 0 ? 0 : _threads;
			}

		int *Cluster(vector<GENERIC_TYPE> data)
			{		
			int *_clusters 				= new int[data.size()];
			vector<GENERIC_TYPE> _centroids 	= vector<GENERIC_TYPE>(_count);
			if(_threads > 0)
				{			
				_step 				= (int)data.size() / _threads;
				_step				+= data.size() % _threads;
				_tList				= new thread[_threads];
				}

			map<int, int> alreadyused;
			/* Set initial centroids by random */
			for(int i = 0; i < _count; i++)
				{
				int bla;
				while(1)
					{
					bla = rand() % data.size();
					if(alreadyused.find(bla) == alreadyused.end())
						{
						break;
						}
					}
				
				_centroids[i] 				= data[bla];
				alreadyused[bla] 			= bla;
				}
			for(int i = 0; i < _iter; i++)
				{
				/* For all points choose the nearest centroid */
				TimeAggStart("Calculating distance");

				if(_threads != 0)
					{
					doParallel(_threads, &data, &_centroids, _clusters);				
					}
				else
					{
					for(int j = 0; j < data.size(); j++)
						{					
						_clusters[j] 		= Nearest(data[j], _centroids);
						WriteProgress(i, _iter, j, this, true);
						}
					}
				TimeAggStop("Calculating distance");

				/* Recalculate centroid mean */
				TimeAggStart("Calculating centroids");
				if(i != (_iter-1))
				{
					_centroids				= _calCentroids->Mean(data, _clusters, _count);
				}
				TimeAggStop("Calculating centroids");
				}			
			return _clusters;
			}	

	private:
		IDistance<GENERIC_TYPE> *_distance;
		IMean<GENERIC_TYPE> 	*_calCentroids;
		int			 _count;
		int			 _iter;
		thread 			*_tList;
		int			 _step;
		int			 _threads;
		
		void paral(vector<GENERIC_TYPE> *elements, vector<GENERIC_TYPE> *centroids, int *clusters, int start, int end)
			{
			for(int i = start; i < end; i++)
				{
				clusters[i] = Nearest(elements->at(i), *centroids);
				}
			} 
		
		void doParallel(int threads, vector<GENERIC_TYPE> *elements, vector<GENERIC_TYPE> *centroids, int *clusters)
			{
			int start = 0;
			int pos = _step;
			int diff;	
			for(int i = 0; i < threads; i++)
				{
				diff = elements->size() - pos;
				if(diff < _step)
					{
					_tList[i] = thread(&CentroidCluster::paral, this, elements, centroids, clusters, start, (start+diff));	
					}
				else
					{				
					_tList[i] = thread(&CentroidCluster::paral, this, elements, centroids, clusters, start, pos);	
					start = pos;
					pos += _step;
					}
				}
			for(int i = 0; i < threads; i++)
				_tList[i].join();
			}

		// Find the nearest centroid 
		// For a squence
		int Nearest(GENERIC_TYPE p, vector<GENERIC_TYPE> centroids)
			{
			int c = 0;			
			double temp, dis = _distance->GetDistance(p, centroids[0]); 
			for(int i = 1; i < _count; i++)
				{ 				
				temp = _distance->GetDistance(p, centroids[i]);	
				if (temp<dis)
					{
					c 	= i;
					dis = temp;
					}
				}
			return c;
			}		
	};
