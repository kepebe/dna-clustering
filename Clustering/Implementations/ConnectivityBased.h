using namespace std;

template<typename GENERIC_TYPE>

class ConnectivityCluster : public ICluster<GENERIC_TYPE>
	{
	public:
		ConnectivityCluster(int (*order)(GENERIC_TYPE, GENERIC_TYPE) = NULL)
			{
				_order = order;
			}
			
		void Initialize(IDistance<GENERIC_TYPE>* dist, Parameter* params) 
			{
			_threshold = 1 - params->GetParameter(0, 0.97);
			_dst = dist;
			}
		
		int* Cluster(vector<GENERIC_TYPE> data)
			{
			int* clusters = new int[data.size()];
			int* indices = GenerateIndices(&data);
			vector<GENERIC_TYPE*> reps; /*represents*/
			int clusterCounter = 0;

			if (_order)
				{
				TimeProbeStart("Sorting input");
				QuickSort(&data, indices);
				TimeProbeEnd();
				}
			/*first cluster*/
			reps.push_back(&data[indices[0]]);
			clusters[0] = clusterCounter; // cluster set, first cluster initiated and added here
			clusterCounter++;
			/*Scan for matches*/
			for(int i = 1; i < data.size(); i++)
				{
				int match = ClusterMatch(data[indices[i]], &reps);
				
				if(match == -1) 
					{
					reps.push_back(&data[indices[i]]);
					clusters[indices[i]] = clusterCounter; // Adding to cluster-set
					clusterCounter++;
					//printf("match: %d ClusterCount = %d\n", match, clusterCounter);
					} 
				else 
					{
					clusters[indices[i]] = match;
					}
				WriteProgress(i, data.size(), reps.size(), this);
				}
			cout << endl;
			//delete[] indices ?????
			return clusters; // returning cluster sets
			}	

	private:
		double _threshold;	
		int (*_order)(GENERIC_TYPE, GENERIC_TYPE);
		
		IDistance<GENERIC_TYPE> *_dst;
		
		int* GenerateIndices(vector<GENERIC_TYPE>* arr)
			{
			int* result = new int[arr->size()];
			for (int i = 0; i < arr->size(); i++)
				{
				result[i] = i;
				}
			return result;
			}
			
		void QuickSort(vector<GENERIC_TYPE>* arr, int* indices)
			{
			quickSortSub(arr, indices, 0, arr->size()-1);
			}
			
		void quickSortSub(vector<GENERIC_TYPE>* arr, int* result, int left, int right)
			{
			int i = left, j = right;
			int tmp;
			GENERIC_TYPE pivot = arr->at(result[(left + right) / 2]);

			while (i <= j)
				{
				while (_order(arr->at(result[i]), pivot) < 0)
					{
					i++;
					}
				while (_order(arr->at(result[j]), pivot) > 0)
					{
					j--;
					}
				if (i <= j)
					{
					tmp = result[i];
					result[i] = result[j];
					result[j] = tmp;
					i++;
					j--;
					}
				}

			if (left < j)
				{
				quickSortSub(arr, result, left, j);
				}
			if (i < right)
				{
				quickSortSub(arr, result, i, right);
				}
			}

		/*Assert match*/
		int ClusterMatch(GENERIC_TYPE v, vector<GENERIC_TYPE*>* reps)
			{
			double t; 
			for(int i = reps->size() - 1; i >= 0; i--) 
				{
				t = _dst->GetDistance(v, *reps->at(i));
				if(t < _threshold)
					{
					return i;			
					}
				}
			return -1; //return cluster
			}
	};
