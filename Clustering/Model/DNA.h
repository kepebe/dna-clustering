#include <string.h>
#include "../Utilities/CharCode.h"

/* The DNA reader assumes the input is a FOSTA file using only one line per DNA string
*  It reads every line not beginning with '>' and converts the result to uppercase
*  It will cause an exception if a DNA string is less than 50 characters
*  The first four characters of each string is an integer that is equal to the string's length */
class FostaFileReader : IDataReader<char*>
	{
	public:
		int Split = -1;
	
		vector<char*> ReadData(ifstream* input)
			{
			vector<char*> result;
			string line;
			int j = 0;
			while(!input->eof() && j != Split)
				{
				getline(*input, line);
				if (line[0] != 0 && line[0] != '>')
					{
					int len = strlen(line.c_str());
					if (len < 50)
						{
						cout << "Found too small DNA at line " << j << endl << line << endl;
						exit(0);
						}
					else if (len > 4000)
						{
						cout << "Found too large DNA at line " << j << endl << line << endl;
						exit(0);
						}
					char* dna = new char[len+4];
					*((int*)dna) = len;
					for (int i = 0; i < len; i++)
						{
						dna[i+4] = FixCode(toupper(line[i]));
						}
					result.push_back(dna);
					j++;
					}
				}
			return result;
			}
	};

 
/**
 * DNAMean create centroids equal to the count of clusters
 * it then assigns for each centroid, 
 * the charactor which is used the most in the sequences in that specific cluster.
 * This process is done until all positions of each cluster has been assigned a charactor.
 * */
class DNAMean : public IMean<char*>
	{
	public:
		vector<char*> Mean(vector<char*> data, int *clusters, int count)
			{		
			// Output centroids
			vector<char*> output;			

			// Vote container
			int charCounter[count][16];

			// Winning charactor
			int winner[count];
			char w[count];
			
			// Set outlength as length
			// of the first sequence
			char *outerc = data[0];
			int outerlen = *((int*)outerc);

			// Initialize winners &
			// Centroids
			for(int m = 0; m < count; m++)
				{
				winner[m] = 0;
				w[m] = 1;
				
				output.push_back(new char[outerlen+4]);
				*((int*)output[m]) = outerlen;
				}

			for(int i = 4; i < outerlen+4; i++)
				{
				// For each sequence in a cluster
				// Vote for charactor i
				// if sequnce.length >= i
				for(int j = 0; j < data.size(); j++)
					{
					int cluster = clusters[j];
					char *innerc = data[j];
					int innerlen = *((int*)innerc);
					if(innerlen >= i)
						{
						charCounter[cluster][(((int)innerc[i]))] += 1;
						if(charCounter[cluster][((int)innerc[i])] > winner[cluster])
							{
							winner[cluster] = charCounter[cluster][((int)innerc[i])];
							w[cluster] = innerc[i];
							}
						}
					}
				// For each cluster
				// Assign the winning charactor at position i
				for(int k = 0; k < count; k++)
					{
					(output[k])[i] = w[k];
					w[k] = 1;
					winner[k] = 0;

					// Clear all votes for cluster k
					for(int l = 1; l < 16; l++)
						{
						charCounter[k][l] = 0;
						}
					}
				}

			return output;
			}			
	};
	
/* Counts the amount of differences between two DNA string
*  Uses the length of the shortest string as the number of chars to compare */
class DNADistance : public IDistance<char*>
	{
	public:
		double GetDistance(char* t1, char* t2)
			{
			int diff = 0;
			int len1 = *((int*)t1);
			int len2 = *((int*)t2);
			int len = min(len1, len2);
			int d = abs((len1-len2));
			for (int i = 4; i < len; i++)
				{
				if ((t1[i] & t2[i]) == 0)
					{
					diff++;
					}
				}
			return diff+d;
			}
	};
	
class DNARelativeDistance : public IDistance<char*>
	{
	public:
		double GetDistance(char* t1, char* t2)
			{
			int diff = 0;
			int len1 = *((int*)t1);
			int len2 = *((int*)t2);
			int len = min(len1, len2);
			for (int i = 4; i < len; i++)
				{
				if ((t1[i] & t2[i]) == 0)
					{
					diff++;
					}
				}
			return (diff/(double)len);
			}
	};

class DNADynamicDistance : public IDistance<char*>
	{
	private:
		int c[2][4000];

	public:
		double GetDistance(char* t1, char* t2)
			{
			int len1 = *((int*)t1);
			int len2 = *((int*)t2);
			int len = max(len1, len2);

			for(int h = 0; h < len+1; h++)
				{
				c[0][h] = 0;
				c[1][h] = 0;
				}
			int flag = 0;
			for(int i = 0; i < len1; i++)
				{
				int* a = c[1 - flag];
				int* b = c[flag];
				for(int j = 0; j < len2; j++)
					{
					if(t1[i+4] & t2[j+4])
						{
						b[j+1] = a[j]+1;
						}	
					else if(a[j+1] >= b[j])
						{
						b[j+1] = a[j+1];
						}
					else
						{
						b[j+1] = b[j];
						}
					}
				c[1-flag][0] = c[flag][0];
				flag = 1 - flag;
				}
			double res = (double)max(c[0][len2], c[1][len2]) / (double)len;
			return 1 - res;
			}
	};
	
void LogDNA(char* dna)
	{
	int len = *((int*)dna);
	for (int i = 4; i < len; i++)
		{
		cout << dna[i];
		}
	cout << endl;
	}

int OrderDNA(char* t1, char* t2)
	{
	int len1 = *((int*)t1);
	int len2 = *((int*)t2);
	int len = min(len1, len2);
	for (int i = 4; i < len; i++)
		{
		if (t1[i] < t2[i])
			{
			return -1;
			}
		if (t1[i] > t2[i])
			{
			return 1;
			}
		}
	if (len1 == len2)
		{
		return 0;
		}
	return (len1 < len2 ? -1 : 1);
	}
	
