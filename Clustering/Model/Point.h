#include <math.h>

using namespace std;

typedef struct point
	{
	int x;
	int y;
	
	bool operator==(const point& a) const
    	{
        return (x == a.x && y == a.y);
    	}
	} Point;
	
class PointMean : public IMean<Point>
	{
	public:
		vector<Point> Mean(vector<Point> data, int* clusters, int count)
			{
			vector<Point> output = vector<Point>(count);
			int clustercount[count];
			for (int i = 0; i < count;i++)
				{
				output[i].x = 0;
				output[i].y = 0;
				clustercount[i] = 0;
				}
			for (int i = 0;i < data.size();i++)
				{
				int cluster = clusters[i];
				output[cluster].x += data[i].x;
				output[cluster].y += data[i].y;
				clustercount[cluster]++;
				}
			for (int i = 0; i < count;i++)
				{
				output[i].x /= clustercount[i];
				output[i].y /= clustercount[i];
				}
			return output;
			}
	};

class EuclidianDistance : public IDistance<Point>
	{
	public:
		double GetDistance(Point t1, Point t2)
			{
			double dx = (double) t1.x - (double) t2.x;
			double dy = (double) t1.y - (double) t2.y;
			return sqrt(dx*dx+dy*dy);
			}
	};
	
/* Splits the string s, using the delimiter delim */
vector<string> &split(const string &s, char delim, vector<string> &elems)
	{
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim))
		{
		elems.push_back(item);
		}
	return elems;
	}

vector<string> split(const string &s, char delim)
	{
	vector<string> elems;
	split(s, delim, elems);
	return elems;
	}
	
/* Reads points based on a list of coordinates separated by newline
*  Each coordinate must be separated by a comma with no whitespace */
class PointFileReader : IDataReader<Point>
	{
	public:
		vector<Point> ReadData(ifstream* input)
			{
			vector<Point> result;
			string line;
			vector<string> coords;

			while(!input->eof())
				{
				*input >> line;
				coords = split(line, ',');
				Point p;
				p.x = atoi(coords[0].c_str());
				p.y = atoi(coords[1].c_str());
				result.push_back(p);
				}

			return result;
			}
	};


