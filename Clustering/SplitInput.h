//Split input op i chunks af X og load dem Y ad gangen
//Kør Y tråde med en chunk per hver
//Saml chunksne til sidst
//Forfra

/* Makes clusters contiguous and returns amount of clusters
** Outliers are put into new clusters */
int NormalizeClusters(int* clusters, int count)
	{
	unordered_map<int, int> clusterMap;
	int num = 0;
	for (int i = 0; i < count; i++)
		{
		if (clusters[i] == -1)
			{
			clusters[i] = num;
			num++;
			}
		else
			{
			unordered_map<int, int>::iterator result = clusterMap.find(clusters[i]);
			if (result == clusterMap.end())
				{
				clusterMap[clusters[i]] = num;
				clusters[i] = num;
				num++;
				}
			else
				{
				clusters[i] = result->second;
				}
			}
		}
	return num;
	}
	
void ReplaceClusters(int* oldC, int* newC, int offset, int count)
	{
	for (int i = 0; i < count; i++)
		{
		oldC[i] = newC[oldC[i]+offset];
		}
	}
	
vector<char*> MergeClusters(int* cluster1, int* cluster2, vector<char*> means1,
					vector<char*> means2, int* count1,
					int count2, ICluster<char*>* clustering, IMean<char*>* mean,
					int** finalResult)
	{
	
	int mc = means1.size();
	means1.insert(means1.end(), means2.begin(), means2.end());
	int* clusters = clustering->Cluster(means1);
	
	int count = NormalizeClusters(clusters, means1.size());
	vector<char*> newMeans = mean->Mean(means1, clusters, count);

	ReplaceClusters(cluster1, clusters, 0, *count1);
	ReplaceClusters(cluster2, clusters, mc, count2);

	int* result = new int[*count1 + count2];
	copy(cluster1, cluster1 + *count1, result);
	copy(cluster2, cluster2 + count2, result + *count1);
	*finalResult = result;
	*count1 = *count1 + count2;
	return newMeans;
	}
	
typedef struct
	{
	vector<char*> data;
	ICluster<char*>* clustering;
	IMean<char*>* mean;
	vector<char*> means;
	int* clusters;
	int size;
	} ThreadData; 
	
ThreadData* InitThreadData(vector<char*> data, ICluster<char*>* clustering, IMean<char*>* mean)
	{
	ThreadData* result = new ThreadData();
	result->data = data;
	result->clustering = clustering;
	result->mean = mean;
	result->size = data.size();
	return result;
	}
	
void ClusterThread(ThreadData* data)
	{
	TimeAggStart("Individal clustering");
	data->clusters = data->clustering->Cluster(data->data);
	TimeAggStop("Individal clustering");
	int count = NormalizeClusters(data->clusters, data->data.size());
	data->means = data->mean->Mean(data->data, data->clusters, count);
	}
	
void MergeThread(ThreadData* data1, ThreadData* data2, ICluster<char*>* mergeClustering)
	{
	data1->means = MergeClusters(data1->clusters, data2->clusters, data1->means,
						data2->means, &(data1->size), data2->size, mergeClustering, data1->mean,
						&(data1->clusters));
	}
	
int PowInt(int x, unsigned int y)
	{
	if (y == 0)
		{
		return 1;
		}
	int result = x;
	for (int i = 0; i < y - 1; i++)
		{
		result *= x;
		}
	return result;
	}
	
void RunSplit(int split, ifstream* input, ICluster<char*>* clustering, ICluster<char*>* mergeClustering, int parallel)
	{
	FostaFileReader reader;
	reader.Split = split;
	thread threads[parallel];
	ThreadData* data[parallel];
	IMean<char*>* mean = new DNAMean();
	
	bool first = true;
	int* resultCluster;
	vector<char*> resultMeans;
	int resultCount = 0;
	int activeThreads = 0;
	TimeProbeStart("Clustering", true);
	int k = 1;
	while (!input->eof())
		{
		TimeProbeStart("Clustered chunks from " + to_string(k) + " to " + to_string(k + parallel));
		for (int i = 0; i < parallel; i++)
			{
			vector<char*> fdata = reader.ReadData(input);
			if (fdata.size() > 0)
				{
				data[i] = InitThreadData(fdata, clustering, mean);
				threads[i] = thread(ClusterThread, data[i]);
				activeThreads++;
				}
			else
				{
				break;
				}
			}
		
		for (int i = 0; i < activeThreads; i++)
			{
			threads[i].join();
			}
		
		int threadMake = activeThreads / 2;
		int j = 0;
		
		while (threadMake > 0)
			{
			for (int i = 0; i < threadMake; i++)
				{
				int p = i * PowInt(2, j + 1);
				threads[i] = thread(MergeThread, data[p], data[p + PowInt(2, j)], mergeClustering);
				}
			
			for (int i = 0; i< threadMake; i++)
				{
				threads[i].join();
				}
				
			j++;
			threadMake /= 2;
			}
			
		if (activeThreads % 2 == 1 && activeThreads > 1)
			{
			threads[0] = thread(MergeThread, data[0], data[activeThreads - 1], mergeClustering);
			threads[0].join();
			}
			
		if (first)
			{
			resultCluster = data[0]->clusters;
			resultMeans = data[0]->means;
			resultCount = data[0]->size;
			first = false;
			}
		else
			{
			resultMeans = MergeClusters(resultCluster, data[0]->clusters, resultMeans,
							data[0]->means, &resultCount, data[0]->size, mergeClustering, mean,
							&resultCluster);
			}
		for (int i = 0; i < activeThreads; i++)
			{
			delete data[i];
			}
		k += activeThreads;
		activeThreads = 0;
		TimeProbeEnd();
		}
	TimeProbeEnd(true);
	OutputResult(resultCluster, resultCount);
	}
	
