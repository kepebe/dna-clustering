using namespace std;

class Parameter
	{
	public:
		Parameter(vector<double> parameters)
			{
			Parameters = parameters;
			}
	
		double GetParameter(int i, double val)
			{
			if (Parameters.size() > i)
				{
				return Parameters[i];
				}
			return val;
			}
	private:
		vector<double> Parameters;
	};
