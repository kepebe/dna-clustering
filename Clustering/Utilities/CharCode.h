char FixCode(char input)
	{
	switch (input)
		{
		case 'T':
		case 'U':
			return 2;
		case 'A':
			return 1;
		case 'G':
			return 4;
		case 'C':
			return 8;
		case 'K':
			return 6;
		case 'M':
			return 9;
		case 'R':
			return 5;
		case 'Y':
			return 10;
		case 'S':
			return 12;
		case 'W':
			return 3;
		case 'B':
			return 14;
		case 'V':
			return 13;
		case 'H':
			return 11;
		case 'D':
			return 7;
		case 'X':
		case 'N':
			return 15;
		}
	throw runtime_error("Got invalid character when loading DNA: " + string(1,input));
	}
