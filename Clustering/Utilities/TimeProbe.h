#include <stdexcept>
#include <time.h>
#include <unordered_map>

using namespace std;

typedef struct timenode
	{
	string Message;
	double TimeStamp;
	timenode* Next;
	} TimeNode;
	
typedef struct
	{
	double TimeStamp;
	double Sum;
	} TimeAgg;

TimeNode* CurrentTimeNode;

unordered_map<string, TimeAgg> TimeAggregate;

double GetTime()
	{
	timespec time;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time);
	return time.tv_sec*1000.0 + time.tv_nsec/1000000.0;
	}

void TimeAggStart(string message)
	{
#ifdef DEBUG
	unordered_map<string, TimeAgg>::iterator result = TimeAggregate.find(message);
	if (result == TimeAggregate.end())
		{
		TimeAgg newNode;
		newNode.TimeStamp = GetTime();
		newNode.Sum = 0;
		TimeAggregate[message] = newNode;
		}
	else
		{
		result->second.TimeStamp = GetTime();
		}
#endif
	}
	
void TimeAggStop(string message)
	{
#ifdef DEBUG
	unordered_map<string, TimeAgg>::iterator result = TimeAggregate.find(message);
	if (result == TimeAggregate.end())
		{
		throw runtime_error("Tried to end time aggregation with no timestamp available!");
		}
	else
		{
		result->second.Sum += GetTime()-result->second.TimeStamp;
		}
#endif
	}
	
void WriteAllTimeAgg()
	{
#ifdef DEBUG
	for (unordered_map<string, TimeAgg>::iterator it = TimeAggregate.begin(); it != TimeAggregate.end(); ++it)
		{
    	cout << it->first << " took " << it->second.Sum << " milliseconds" << endl;
    	}
#endif
	}

void TimeProbeStart(string message, bool force = false)
	{
#ifndef DEBUG
	if (force)
		{
#endif
		TimeNode* node = new TimeNode();
		node->Message = message;
		node->TimeStamp = GetTime();
		node->Next = CurrentTimeNode;
		CurrentTimeNode = node;
#ifndef DEBUG
		}
#endif
	}
	
void TimeProbeEnd(bool force = false)
	{
#ifndef DEBUG
	if (force)
		{
#endif
	TimeNode* node = CurrentTimeNode;
	if (node == NULL)
		{
		throw runtime_error("Tried to end time probe with no probe on stack!");
		}
	CurrentTimeNode = node->Next;
	double time = GetTime() - node->TimeStamp;
	cout << node->Message << ": " << time << " milliseconds" << endl;
	delete(node);
#ifndef DEBUG
		}
#endif
	}
