using namespace std;

template<typename T>
void WriteProgress(int done, int count, int clusters, ICluster<T>* cluster, bool alternate = false)
	{
	if (cluster->Multithreaded)
		{
		return;
		}
	done++;
	clusters++;
	if ((done%100==0 && !alternate) || (alternate && clusters%100==0))
		{
		if (alternate)
			{
			cout << clusters << " clustered in iteration " << done << " out of " << count << '\r';
			}
		else
			{
			cout << done << " out of " << count << " with " << clusters << " clusters" << '\r';
			}
		
		flush(cout);
		}
	if (done == count + 1)
		{
		cout << endl;
		}
	};
	
void OutputResult(int* groups, int count)
	{
	ofstream outStream;
	outStream.open(outputFileName);
	for (int i = 0; i < count; i++)
		{
		outStream << groups[i] << endl;
		}
		
	outStream.close();
	}
	
vector<double> parameters1, parameters2;
	
void LoadParameters(string input, vector<double>* output)
	{
	vector<string> result = split(input, ',');
	for (int i = 0; i<result.size();i++)
		{
		output->push_back(stod(result[i]));
		}
	}
