#include <sstream>
#include <string>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <vector>

using namespace std;

bool Error = false;

vector<string> ReadData(string file)
	{
	ifstream inStream;
	inStream.open(file);
	
	vector<string> result;
	string line;

	while(!inStream.eof())
		{
		getline(inStream, line);
		if (line[0] != 0)
			{
			result.push_back(line);
			}
		}
	inStream.close();
	return result;
	}

string exec(const char* cmd)
	{
	FILE* pipe = popen(cmd, "r");
	if (!pipe) return "ERROR";
	char buffer[128];
	string result = "";
	while(!feof(pipe))
		{
		if(fgets(buffer, 128, pipe) != NULL)
		result += buffer;
		}
	Error = pclose(pipe) != 0;
	return result;
	}

int main(int argc, char* argv[])
	{
	vector<string> commands = ReadData(argv[1]);
	int start = 0;
	if (argc > 2)
		{
		start = atoi(argv[2]);
		}
	for (int i = start; i < commands.size(); i++)
		{
		cout << "Running command: " << commands[i] << " - " << (i+1) << " out of " << commands.size() << endl;
		string result = exec(commands[i].c_str());
		if (Error)
			{
			cout << "Quitting due to error" << endl;
			break;
 			}
		ofstream outStream;
		outStream.open(to_string(i + 1) + ".txt");
		outStream << result;
		outStream.close();
		}
	}
