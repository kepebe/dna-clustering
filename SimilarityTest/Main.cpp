#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <cmath>
#include "../Clustering/Utilities/TimeProbe.h"
#include "../Clustering/Utilities/CharCode.h"

using namespace std;

/* Kenneth - CSVClustering */
vector<vector<string> > GroupDNA(vector<string> data, vector<int> groups)
        {
        map<int, int> uniqueGroups;
        int next = 0;
        for (int i = 0; i < groups.size(); i++)
                {
                map<int, int>::iterator result = uniqueGroups.find(groups[i]);
                if (result == uniqueGroups.end())
                        {
                        uniqueGroups[groups[i]] = next;
                        next++;
                        }
                }

        vector<vector<string> > result(uniqueGroups.size());

        for (int i = 0; i < data.size(); i++)
                {
                result[uniqueGroups[groups[i]]].push_back(data[i]);
                }

        return result;
        }

/* Kenneth - CSVClustering */
vector<string> ReadData(char filename[])
        {
        ifstream input(filename);
        vector<string> result;
        string line;
        while(!input.eof())
                {
                getline(input, line);
                if (line[0] != 0 && line[0] != '>')
                        {
						for(int i = 0; i < line.length(); i++)
							{
							line[i] = FixCode(toupper(line[i]));
							}                        
						result.push_back(line);
                        } 
                }
        input.close();
        return result;
        }

/* Kenneth - CSVClustering */
vector<int> LoadGroups(char Filename[])
        {
        vector<int> result;
        ifstream in_stream;

        string line;
        vector<string> coords;

        in_stream.open(Filename);

        while(!in_stream.eof())
                {
                in_stream >> line;
                result.push_back(atoi(line.c_str()));
                }

        in_stream.close();

        return result;
        }

/* Return den mindste længde af to strenge
 * Hvis lige lange, return da, den første */
int dnaMin(string t1, string t2)
	{
	return t1.length() <= t2.length() ? t1.length() : t2.length();
	}

/* Find antal ens karaktere */
int distance(string x1, string x2)
	{
	int diff = 0; 
	int len = dnaMin(x1, x2);
	for(int i = 0; i < len; i++)
		if((x1[i] & x2[i]) == 0)
			diff++;
	return diff;
	}

/* Find forksellen på to strenge i procent # 0.xx */
double similar(string x1, string x2)
	{
	int d = distance(x1, x2);
	return 1 - ((double)d / dnaMin(x1, x2));
	}
	
double* MinAverageMax(vector<int> list)
	{
	double* result = new double[3];
	result[0] = 999999;
	result[1] = 0;
	result[2] = 0;
	for (int i = 0; i < list.size(); i++)
		{
		result[0] = result[0] < list[i] ? result[0] : list[i];
		result[1] += list[i];
		result[2] = result[2] > list[i] ? result[2] : list[i];
		}
	result[1] = result[1] / list.size();
	return result;
	}

int main(int argc, char* argv[])
        {
	// Opdel grupper efter filerne
	// input.txt og output.txt
        TimeProbeStart("Grouping");
	vector<string>                  data = ReadData(argv[1]);
        vector<int>                     groups = LoadGroups(argv[2]);
        vector<vector<string> > result = GroupDNA(data, groups);
	TimeProbeEnd();

	// Mindste similarity for én enkelt sekvens
	double indiMin;	
	
	// Mindste similarity for én gruppe
	double grMin;

	// Største forskel på to sekvenser
	double similarity = 0.96;

	// Debugging
	// Optæller sekvenser der opfylder similarity
	int match;
	
	int correct = 0;
	int wrong = 0;
	int singles = 0;
	
	// En temp vektor
	// Der indeholde en enkelt gruppe
	vector<string> g;

	// Break outerloop
	int stopLoop;
	
	vector<int> elementCount;

	// Nedenstående løkker
	// Indeholder en masse variable
	// De bruges kun til at få et 
	// Overblik hvilken similarity 
	// Der stopper gruppe løkken

	// Gruppe løkke
	
	cout << result.size() << " clusters in file" << endl;
	
	for(int i = 0; i < result.size(); i++)
		{
		g = result[i];
		grMin = 1;	
		indiMin = 1;
		match = 0;
		stopLoop = -1;
		if (g.size() == 1)
			{
			singles++;
			correct++;
			continue;
			}
		else
			{
			elementCount.push_back(g.size());
			}
		
		// Individuel løkke
		for(int j = 0; j < g.size(); j++)
			{
			if(stopLoop == 1)
				{
				break;
				}
			// Forskels beregner
			for(int k = j + 1; k < g.size(); k++)
				{				
				double is;

				// Beregn forskellen
				// Mellem to sekvenser
				TimeAggStart("Indi_sim");
				if((is = similar(g[j], g[k])) < indiMin)
					{
					indiMin = is;			
					}
				TimeAggStop("Indi_sim");
				
				// Forskel mellem to sekvenser
				// Er større end similarity tillader
				if(indiMin < similarity && indiMin > 0)
					{
					stopLoop = 1;
					break;
					}
				}
			match++;
			if(indiMin < grMin)
				{
				grMin = indiMin;
				}
			}
		cout << "Group " << i << ": matches " << match << " of " << g.size() << " : " << grMin << '\r';
		flush(cout);
		if (grMin < similarity)
			{
			wrong++;
			}
		else
			{
			correct++;
			}
		}
	cout << correct << " correct, " << wrong << " wrong, " << singles << " clusters with only one item" << endl;
	double* values = MinAverageMax(elementCount);
	cout << "Not counting one-element groups, min: " << values[0] <<
			", average: " << values[1] << ", max: " << values[2] << endl;
 	WriteAllTimeAgg();      
	return 0;
	}
