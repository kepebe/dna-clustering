#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <X11/Xlib.h>

using namespace std;

typedef struct
	{
	int x;
	int y;
	} Point;

XColor* GetMainColors(Display* display)//RGB
	{
	XColor* result = new XColor[7];
	char colors[][8] = {"#000000","#FF0000","#00FF00","#0000FF","#FFFF00","#00FFFF","#FF00FF"};
	Colormap colormap = DefaultColormap(display, 0);

	for (int i=0;i<7;i++)
		{
		XParseColor(display, colormap, colors[i], &result[i]);
		XAllocColor(display, colormap, &result[i]);
		}

	return result;
	}

void DrawPoint(Display* display, Window frame_window, GC graphical_context, int x, int y, XColor color)
	{
	XSetForeground(display, graphical_context, color.pixel);
	XDrawArc(display, frame_window, graphical_context, x,y,4,4,0,360*64);
	}

vector<string> &split(const string &s, char delim, vector<string> &elems)
	{
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim))
		{
		elems.push_back(item);
		}
	return elems;
	}


vector<string> split(const string &s, char delim)
	{
	vector<string> elems;
	split(s, delim, elems);
	return elems;
	}

vector<Point> LoadPoints(char Filename[])
	{
	vector<Point> result;
	ifstream in_stream;

	string line;
	vector<string> coords;

	in_stream.open(Filename);

	while(!in_stream.eof())
		{
		in_stream >> line;
		coords = split(line, ',');
		Point p;
		p.x = atoi(coords[0].c_str());
		p.y = atoi(coords[1].c_str());
		result.push_back(p);
		}

	in_stream.close();

	return result;
	}

vector<int> LoadGroups(char Filename[])
	{
	vector<int> result;
	ifstream in_stream;

	string line;
	vector<string> coords;

	in_stream.open(Filename);

	while(!in_stream.eof())
		{
		in_stream >> line;
		result.push_back(atoi(line.c_str()));
		}

	in_stream.close();

	return result;
	}


int main(int argc, char* argv[])
	{
	vector<Point> 			points = LoadPoints(argv[1]);
	vector<int> 			groups = LoadGroups(argv[2]);
	Display*				display;
	Visual*					visual;
	int						depth;
	XSetWindowAttributes	frame_attributes;
	Window					frame_window;
	XFontStruct*			fontinfo;
	XGCValues				gr_values;
	GC						graphical_context;
	XEvent					event;
	XColor*					colors;

	display = XOpenDisplay(NULL);
	visual = DefaultVisual(display, 0);
	depth  = DefaultDepth(display, 0);

	frame_attributes.background_pixel = XWhitePixel(display, 0);
	frame_window = XCreateWindow(display, XRootWindow(display, 0),
			0, 0, 400, 400, 5, depth,
			InputOutput, visual, CWBackPixel,
			&frame_attributes);
	XStoreName(display, frame_window, "Cluster draw");
	XSelectInput(display, frame_window, ExposureMask | StructureNotifyMask);

	fontinfo = XLoadQueryFont(display, "10x20");
	gr_values.font = fontinfo->fid;
	gr_values.foreground = XBlackPixel(display, 0);
	graphical_context = XCreateGC(display, frame_window,
			GCFont+GCForeground, &gr_values);
	XMapWindow(display, frame_window);
	Atom wmDeleteMessage = XInternAtom(display, "WM_DELETE_WINDOW", False);
	XSetWMProtocols(display, frame_window, &wmDeleteMessage, 1);

	colors = GetMainColors(display);

	bool running = true;
	while ( running )
		{
		XNextEvent(display, (XEvent *)&event);
		switch ( event.type )
			{
			case Expose:
				{
				for(vector<Point>::size_type i = 0; i != points.size(); i++)
					{
					int group = groups[i];
					DrawPoint(display, frame_window, graphical_context, points[i].x, points[i].y, group == -1 ? colors[0] : colors[(groups[i] % 6) + 1]);
					}
				break;
				}
			case ClientMessage:
				if(event.xclient.data.l[0] == wmDeleteMessage) 
					{
					running=false;
					}
				break;
			default:
			break;
			}
		}
	XDestroyWindow(display, frame_window);
	XCloseDisplay(display);
	return 0;
	}

